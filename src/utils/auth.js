var tokenKey = 'access_token'

export function getToken() {
  return localStorage.getItem(tokenKey) || sessionStorage.getItem(tokenKey)
}

export function setToken(token, remberMe) {
  if (remberMe) {
    localStorage.setItem(tokenKey, token)
  } else {
    sessionStorage.setItem(tokenKey, token)
  }
}

export function removeToken() {
  localStorage.removeItem(tokenKey)
  sessionStorage.removeItem(tokenKey)
}
